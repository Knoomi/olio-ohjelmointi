/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package harjoitus8;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashSet;
import java.util.ResourceBundle;
import java.util.Set;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.Slider;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;

/**
 *
 * @author juho-pekkakoponen
 */
public class FXMLDocumentController implements Initializable {
    
    private Label label;
    @FXML
    private Button addMoneyButton;
    @FXML
    private Button returnMoneyButton;
    @FXML
    private ComboBox<String> bottleCombo;
    @FXML
    private Button buyBottleButton;
    @FXML
    private ComboBox<Double> sizeCombo;
    @FXML
    private Slider changeMoneySlider;
    @FXML
    private TextField changeMoneyField;
    @FXML
    private TextArea outputArea;
    @FXML
    private TextField totalMoneyField;
    @FXML
    private TextField priceField;
    @FXML
    private Button receiptButton;
    
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        BottleDispenser bd = BottleDispenser.getInstance();
        ArrayList<Bottle> bottleArray = BottleDispenser.getBottleArray();
        Set<String> bottleNames = new HashSet<>();
        Set<Double> bottleSizes = new HashSet<>();
        
        bottleArray.stream().map((bottleArray1) -> {
            bottleNames.add(bottleArray1.getName());
            return bottleArray1;
        }).forEach((bottleArray1) -> {
            bottleSizes.add(bottleArray1.getSize());
        });
        bottleCombo.getItems().addAll(bottleNames);
        sizeCombo.getItems().addAll(bottleSizes);
        bottleCombo.getSelectionModel().select(bottleCombo.getItems().get(0));
        sizeCombo.getSelectionModel().select(sizeCombo.getItems().get(0));
        priceField.setText(findBottlePrice(bottleCombo.getItems().get(0),
                sizeCombo.getItems().get(0)));

    }    

    @FXML
    private void addMoneyAction(ActionEvent event) {
        BottleDispenser bd = BottleDispenser.getInstance();
        double addedMoney = Double.parseDouble(changeMoneyField.getText());
        bd.addMoney(addedMoney);
        String totalMoney = BottleDispenser.moneyToString(BottleDispenser.getMoney());
        
        outputArea.setText("Klink! " + BottleDispenser.moneyToString(addedMoney) +
                " € lisätty laitteeseen!"+ "\n" +
                outputArea.getText());
        totalMoneyField.setText(totalMoney);

    }

    @FXML
    private void returnMoneyAction(ActionEvent event) {
        BottleDispenser bd = BottleDispenser.getInstance();
        String outputText = bd.returnMoney();
        
        outputArea.setText(outputText + "\n" +
                outputArea.getText());
        String totalMoneyText = BottleDispenser.moneyToString(BottleDispenser.getMoney());
        totalMoneyField.setText(totalMoneyText);
        
    }

    @FXML
    private void buyBottleAction(ActionEvent event) throws IOException {
        BottleDispenser bd = BottleDispenser.getInstance();
        int buyEvent = bd.buyBottle(bottleCombo.getSelectionModel().getSelectedItem(),
                sizeCombo.getSelectionModel().getSelectedItem());
        
        switch (buyEvent){
            case 0: outputArea.setText("Pullot loppu!\n" + outputArea.getText());
                break;
            case 1: outputArea.setText("KACHUNK! " +
                    bottleCombo.getSelectionModel().getSelectedItem() +
                    " tipahti masiinasta!\n" +
                    outputArea.getText());
                    totalMoneyField.setText(
                            BottleDispenser.moneyToString(BottleDispenser.getMoney()));
                break;
            case 2: outputArea.setText("Syötä rahaa ensin!\n" + outputArea.getText());
                break;
        }
    }

    @FXML
    private void changeMoneyAction(MouseEvent event) {
        double money = (double)(Math.round(changeMoneySlider.getValue()*100))/100;
        
        changeMoneyField.setText(BottleDispenser.moneyToString(money));
    }

    @FXML
    private void bottleAction(ActionEvent event) {
        String price;
        if (sizeCombo.getSelectionModel().getSelectedItem()!=null) {
            price = findBottlePrice(bottleCombo.getSelectionModel().getSelectedItem(),
                    sizeCombo.getSelectionModel().getSelectedItem());
            priceField.setText(price);
        }
    }

    @FXML
    private void sizeAction(ActionEvent event) {
        String price;
        
        if (bottleCombo.getSelectionModel().getSelectedItem() != null) {
            price = findBottlePrice(bottleCombo.getSelectionModel().getSelectedItem(),
                    sizeCombo.getSelectionModel().getSelectedItem());
            priceField.setText(price);
        }
    }
    
    private String findBottlePrice(String name, double size) {
        String price="-";
        BottleDispenser bd = BottleDispenser.getInstance();
        ArrayList<Bottle> bottleArray = BottleDispenser.getBottleArray();
        
        for (Bottle bottleArray1 : bottleArray) {
            if (bottleArray1.getName().equals(name) &&
                    bottleArray1.getSize() == size) {
                price = BottleDispenser.moneyToString(bottleArray1.getPrice());
            }
        }
        return price;
    }

    @FXML
    private void receiptAction(ActionEvent event) throws FileNotFoundException {
        Bottle bottle = BottleDispenser.getLastBuy();
        
        if (bottle!=null) {
            SimpleDateFormat date = new SimpleDateFormat("dd.MM.YYYY");
            SimpleDateFormat time = new SimpleDateFormat("HH:mm");
            Calendar cal = Calendar.getInstance();
            PrintWriter writer = new PrintWriter("kuitti.txt");
            
            writer.write("Kuitti tulostettu " + date.format(cal.getTime()) +
                    " klo " + time.format(cal.getTime()) + ".\n\n" +
                    bottle.getName() + ", " +
                    bottle.getSize() + "l, \t" +
                    BottleDispenser.moneyToString(bottle.getPrice()) + "€");
            writer.close();
            outputArea.setText("Kuitti tulostettu.\n"+outputArea.getText());
        }
        else {
            outputArea.setText("Kuittia ei voitu tulostaa.\n" + outputArea.getText());

        }
    }
    
} 
    



/* sources:
    http://stackoverflow.com/questions/12806278/double-decimal-formatting-in-java
    http://stackoverflow.com/questions/203984/how-do-i-remove-repeated-elements-from-arraylist
    http://stackoverflow.com/questions/833768/java-code-for-getting-current-time
*/
