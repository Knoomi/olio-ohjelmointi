/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package harjoitus8;

/**
 *
 * @author Juho
 */
public class Bottle {
    private String name;
    private String manufacturer;
    private double total_energy;
    private double size;
    private double price;
    
    public Bottle(){
        name = "Pepsi Max";
        manufacturer = "Pepsi";
        total_energy = 0.3;
        size = 0.5;
        price = 1.8;
    }
    
    public Bottle(String n, String m, double s, double p) {
        name = n;
        manufacturer = m;
        total_energy = 0.3;
        size = s;
        price = p;
    }
    
    public String getName() {
        return name;
    }
    
    public double getPrice() {
        return price;
    }
    
    public double getSize() {
        return size;
    }
    
    /*@Override
    public String toString() {
        return (name+", "+Double.toString(size)+" l");
    }*/
}
