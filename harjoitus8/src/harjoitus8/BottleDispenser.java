/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package harjoitus8;

import java.io.IOException;
import java.text.DecimalFormat;
import java.util.ArrayList;

/**
 *
 * @author Juho
 */
public class BottleDispenser {
    
    private static int bottles;
    private static double money;
    private static ArrayList<Bottle> bottle_array = new ArrayList<Bottle>();
    private static Bottle lastBuy = null;
    private static BottleDispenser bd = null;
    
    
    private BottleDispenser() {
        money = 0;
        
        bottle_array.add(new Bottle());
        bottle_array.add(new Bottle("Pepsi Max", "Pepsi", 1.5, 2.2));
        bottle_array.add(new Bottle("Coca-Cola Zero", "Coca-Cola", 0.5, 2.0));
        bottle_array.add(new Bottle("Coca-Cola Zero", "Coca-Cola", 1.5, 2.5));
        
        for (int i=0;i<2;i++) {
            bottle_array.add(new Bottle("Fanta Zero", "Fanta", 0.5, 1.95));
        }
        bottles = bottle_array.size();
    }

    static public BottleDispenser getInstance() {
        if(bd==null) {
            bd = new BottleDispenser();
        } else {
        }
        return bd;
    }
    
    public void addMoney(double x) {
        money =(double)(Math.round((x+money) * 100)) / 100;
    }

    public int buyBottle(String name, double size) throws IOException {
        String temp = "";
        int i=0;
        int buyEvent=0;
        int bottleAmount=bottle_array.size();
        
        for(i = 0; i<bottleAmount;i++) {
            if ( (bottle_array.get(i).getName().equals(name))
                    && (bottle_array.get(i).getSize()==size)
                    && (bottle_array.get(i).getPrice()<=money) ) {
                money = (double)(Math.round((money-bottle_array.get(i).getPrice()) * 100)) / 100;
                lastBuy = bottle_array.get(i);
                bottle_array.remove(i);
                buyEvent = 1;
                break;
            }
            else if ( (bottle_array.get(i).getName().equals(name))
                    && (bottle_array.get(i).getSize() == size) ) {
                buyEvent = 2;
                break;
            }
        }
        return buyEvent;
    }

    public String returnMoney() {
        String teksti = ("Klink klink. Sinne menivät rahat! Rahaa tuli ulos " +
                moneyToString(money) + "€.");
        money = 0;
        
        return teksti;
    }
    
    public void listBottles() {
        for (int i=0;i<bottles;i++) {
            System.out.print(i+1+". Nimi: " + bottle_array.get(i).getName() +
                    "\n\tKoko: " + bottle_array.get(i).getSize() + "\tHinta: " + bottle_array.get(i).getPrice()+ "\n");
        }
    }
    
    public void removeBottle(int x) {
        bottle_array.remove(x);
    }
    
    public static double getMoney() {
        return money;
    }
    
    public static String moneyToString(double value) {
        DecimalFormat formatter = new DecimalFormat("#0.00");
        
        return formatter.format(value);
    }
    
    public static Bottle getLastBuy() {
        return lastBuy;
    }
    
    /**
     *
     * @return
     */
    public static ArrayList<Bottle> getBottleArray() {
        return bottle_array;
    }
}
