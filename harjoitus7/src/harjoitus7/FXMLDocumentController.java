/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package harjoitus7;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.HBox;

/**
 *
 * @author juho-pekkakoponen
 */
public class FXMLDocumentController implements Initializable {
    
    @FXML
    private Label label;
    @FXML
    private Button button;
    @FXML
    private TextField inputField;
    @FXML
    private Button loadButton;
    @FXML
    private Button saveButton;
    @FXML
    private TextArea textArea;
    @FXML
    private TextField fileNameField;
    @FXML
    private Label fileNoticer;
    
    @FXML
    private void handleButtonAction(ActionEvent event) {
        System.out.println("Hello World!");
        label.setText(inputField.getText());
        inputField.setText(null);

    }
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }    

    @FXML
    private void handleTextFieldAction(KeyEvent event) {
        if (event.getCode().equals(KeyCode.ENTER)) {
            label.setText(inputField.getText());
            inputField.setText(null);
        }
    }

    @FXML
    private void loadButtonAction(ActionEvent event) throws IOException {
        String name = fileNameField.getText();
        File file = new File(name);
        String text = "";
        String line = null;
        BufferedReader r = new BufferedReader(new FileReader(file));
        
        while ((line=r.readLine()) != null) {
            text=text+line+"\n";
        }
        r.close();
        textArea.setText(text);
        fileNoticer.setText("Tiedostosta \""+name+"\" ladattiin onnistuneesti.");
    }

    @FXML
    private void saveButtonAction(ActionEvent event) throws IOException {
        String name = fileNameField.getText();
        String text=textArea.getText();
        File file = new File(name);
        BufferedWriter w = new BufferedWriter(new FileWriter(file));
        w.write(text);
        w.close();
        fileNoticer.setText("Tiedostoon \"" + name + "\" tallennettiin onnistuneesti.");

    }

    

    
    
}
