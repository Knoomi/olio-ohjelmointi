/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package harjoitus9;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.StringReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

/**
 *
 * @author juho-pekkakoponen
 */
public class ListOfTheaters {
    
    private Document doc;
    private HashMap<String, String> map;
    static private ArrayList<Theater> theaterArray = new ArrayList<Theater>();
    static private ListOfTheaters lot = null;
    
    public ListOfTheaters(String content) {
        try {
            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
            doc = dBuilder.parse(new InputSource(new StringReader(content)));
            
            map = new HashMap();
            parseTheaterData();
            
        } catch (ParserConfigurationException | SAXException | IOException ex) {
            Logger.getLogger(ListOfTheaters.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    static public ListOfTheaters getInstance() {
        if (lot == null) {
            try {
                lot = new ListOfTheaters(getURL("http://www.finnkino.fi/xml/TheatreAreas/"));
            } catch (IOException ex) {
                Logger.getLogger(ListOfTheaters.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return lot;
    }
 
    private void parseTheaterData () {
        NodeList nodes = doc.getElementsByTagName("TheatreArea");
        for (int i=2; i<nodes.getLength();i++) {
            Node node = nodes.item(i);
            //String tnode = nodes.item(i).g;
            Element e = (Element)node;
            
            String name = e.getElementsByTagName("Name").item(0).getTextContent();
            String id = e.getElementsByTagName("ID").item(0).getTextContent();

            
            map.put(id, name);
           // map.remove(1029);
            
            Theater theater = new Theater(name, id);
        }
    }
    
    private String getValue(String tag, Element e, String attr) {
        System.out.println("loll");
        return ((Element)e.getElementsByTagName(tag).item(0)).getAttribute(attr);
    }

    public HashMap<String, String> getMap() {
        return map;
    }
    
    static private String getURL(String s) throws MalformedURLException, IOException {
        URL url = new URL(s);
        BufferedReader br = new BufferedReader(new InputStreamReader(url.openStream()));
        String content = "";
        String line;
        
        while ((line=br.readLine())!=null) {
            content+=line+"\n";
        }
        
        return content;
        
        
    }
    
}
