/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package harjoitus9;

/**
 *
 * @author juho-pekkakoponen
 */
public class Theater {
    
    private String name;
    private String ID;
    
    public Theater(String n, String id) {
        name = n;
        ID = id;
    }
    
    public String getName() {
        return name;
    }

    public String getID() {
        return ID;
    }
}
