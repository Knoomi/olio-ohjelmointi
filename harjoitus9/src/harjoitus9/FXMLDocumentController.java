/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package harjoitus9;

import java.io.IOException;
import java.net.URL;
import java.util.Map.Entry;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;

/**
 *
 * @author juho-pekkakoponen
 */
public class FXMLDocumentController implements Initializable {
    
    @FXML
    private Label label;
    @FXML
    private ComboBox<String> theaterCombo;
    @FXML
    private TextField dateField;
    @FXML
    private TextField endTimeField;
    @FXML
    private TextField startTimeField;
    @FXML
    private Button listMoviesButton;
    @FXML
    private Button searchNameButton;
    @FXML
    private ListView<?> moviesList;
    
    private void handleButtonAction(ActionEvent event) {
        System.out.println("You clicked me!");
        label.setText("Hello World!");
    }
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        ListOfTheaters lot = ListOfTheaters.getInstance();
       
        for (Entry<String, String> e : lot.getMap().entrySet()) {
            theaterCombo.getItems().add((e.getValue()));
        }
        
        theaterCombo.getItems().sort(null);

        

    }    

    @FXML
    private void listMoviesAction(ActionEvent event) {
        if (!theaterCombo.getSelectionModel().isEmpty()) {
            
        }
    }

    @FXML
    private void searchNameAction(ActionEvent event) {
    }
    
}
