/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package harjoitus10;

import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.concurrent.Worker.State;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ProgressIndicator;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.web.WebView;

/**
 *
 * @author Juho
 */
public class FXMLDocumentController implements Initializable {
    @FXML
    private WebView webView;
    @FXML
    private TextField addressInputField;
    @FXML
    private Button reloadButton;
    @FXML
    private Button englishButton;
    @FXML
    private Button finnishButton;
    @FXML
    private Button lastButton;
    @FXML
    private Button nextButton;
    @FXML
    private ProgressIndicator progressInd;
    
    private static boolean isBack=false;
    private static boolean isNext=false;
    private static boolean isReload=false;
    
  
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        SavedPages sp = SavedPages.getInstance();
         webView.getEngine().getLoadWorker().stateProperty().addListener(
        new ChangeListener<State>() {
            @Override
            public void changed(ObservableValue ov, State oldState, State newState) {
                if(newState==State.RUNNING) {
                    progressInd.setProgress(-1);
                    progressInd.setVisible(true);
                }
                if (newState == State.SUCCEEDED) {
                    progressInd.setProgress(1);
                    progressInd.setVisible(false);
                    String current = webView.getEngine().getLocation();
                    addressInputField.setText(current);
                    
                    if (isNext == false && isBack == false && isReload==false) {
                        sp.getNextPages().clear();
                        sp.addLastPage(sp.getCurrentPage());
                        sp.setCurrentPage(current);
                    }
                    
                    if (isBack == true) {
                        sp.getLastPages().remove(0);
                        sp.setCurrentPage(current);
                        isBack=false;
                    }
                    
                    if (isNext == true) {
                        sp.getNextPages().remove(0);
                        sp.setCurrentPage(current);
                        isNext=false;
                    }
                    
                    if (isReload == true) {
                        isReload=false;
                    }

                 }
            }
        });
         // https://docs.oracle.com/javafx/2/api/javafx/scene/web/WebEngine.html
    }    

    @FXML
    private void loadPage(ActionEvent event) {
        String lastPage = webView.getEngine().getLocation();
        SavedPages sp = SavedPages.getInstance();
        sp.addLastPage(lastPage);
        if(addressInputField.getText().equals("index.html")) {
            webView.getEngine().load(
                    getClass().getResource("index2.html").toExternalForm());
        }
        else {
            if(addressInputField.getText().startsWith("http://")) {
                webView.getEngine().load(addressInputField.getText());
            }
            else {
                webView.getEngine().load("http://" + addressInputField.getText());
            }

        }
    }

    @FXML
    private void reloadPage(ActionEvent event) {
        isReload=true;
        webView.getEngine().reload();
    }

    @FXML
    private void englishAction(ActionEvent event) {
        webView.getEngine().executeScript("document.shoutOut()");

    }

    @FXML
    private void finnishAction(ActionEvent event) {
        webView.getEngine().executeScript("initialize()");

    }

    @FXML
    private void loadLastPage(ActionEvent event) {
        SavedPages sp = SavedPages.getInstance();
        if(sp.getLastPages().size()!=0) {
            isBack=true;
            sp.addNextPage(webView.getEngine().getLocation());
            webView.getEngine().load((String) sp.getLastPages().get(0));    
        }
        
    }

    @FXML
    private void loadNextPage(ActionEvent event) {
        
        SavedPages sp = SavedPages.getInstance();
        if(sp.getNextPages().size()!=0) {
            isNext=true;
            sp.addLastPage(webView.getEngine().getLocation());
            webView.getEngine().load((String)sp.getNextPages().get(0));
        }
    }

    

    
}
