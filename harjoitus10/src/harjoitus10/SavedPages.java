/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package harjoitus10;

import java.util.ArrayList;

/**
 *
 * @author Juho
 */
public class SavedPages {
    
    private static SavedPages sp = null;
    private static ArrayList<String> lastPages;
    private static ArrayList<String> nextPages;
    private static String currentPage;
    
    private SavedPages() {
        lastPages = new ArrayList<>();
        nextPages = new ArrayList<>();
    }
    
    public static SavedPages getInstance() {
        if (sp==null) {
            sp = new SavedPages();
        }
        return sp;
    }

    public void addLastPage(String lastPage) {
        if (lastPages.size()<10) {
            lastPages.add(0, lastPage);
        }
        else {
            lastPages.remove(9);
            lastPages.add(0, lastPage);
        }
    }

    public void addNextPage(String nextPage) {
        if (nextPages.size() < 10) {
            nextPages.add(0, nextPage);
        } else {
            nextPages.remove(9);
            nextPages.add(0, nextPage);
        }    }

    public ArrayList getLastPages() {
        return lastPages;
    }

    public ArrayList getNextPages() {
        return nextPages;
    }

    public String getCurrentPage() {
        return currentPage;
    }

    public void setCurrentPage(String currentPage) {
        SavedPages.currentPage = currentPage;
    }
    
    
    
}
